<?php declare(strict_types=1); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<h1>Ejercicio 1</h1>

<h3>1.1</h3>
<p>Stric_types sirve para especificar el tipo de dato que admitira los parametros de la funcion, aqui un ejemplo</p>
<p>declare(strict_types=1);<br>
function sumar(int $valor1,int $valor2){<br>
    return $valor1 + valor2;<br>
    }
<br>
    echo sumar(1,2);
    <br>
    echo sumar(1.1,2);


</p>


<?php
function sumar(int $valor1, int $valor2){
    return $valor1 + $valor2;
    }

    var_dump (sumar(1,2));
    echo "<br>";
    //var_dump (sumar(1.1,1.2));



?>

<p>El primer ejemplo mostrara la operacion de forma eficiente pero al introducir otro tipo<br>
 de dato que no ha sido el especificado muestra un error que
  en este caso es por introducir un float<br>
  Uncaught TypeError: Argument 1 passed to sumar() must be of the type int, float given,
  </p>

  <h3>1.2</h3>
    <p>Similar a como se hizo anteriormente pero añadiendo un parametro mas al final especificando el tipo de dato que se desea</p>
  <p>function muestra(int $valor): int</p>
  <?php
  function muestra(int $valor):int{

    return $valor;
  }

  echo "Mostraria lo siguiente en caso de ejecutarlo correctamente";
  echo "<br>";
  echo muestra(23);
  echo "<br>";
  

  
  ?>
  <h3>1.3</h3>
  <p>Para un tipo de valor o null:</p>
  <p> function muestra(int $valor): ?int</p>
  
  
  <h1>Ejercicio 2</h1>
  <?php
  function insert($tablaNom, $datos){

    $sentenciaSQL = "insert into ".$tablaNom." (".implode(",",array_keys($datos)).") values "." (:".implode(", :",$datos).")";


    return $sentenciaSQL;


  }

  $coche=["Matricula"=>"0001","Marca"=>"Mercedes","modelo"=>"AMG Cabrio", "Precio"=>"64100"];
  $tabla="Coches";
  $sql = insert($tabla,$coche);
  var_dump($sql);

?>
<h3>Ejercicio 3</h3>

<?php
    function insertV2($tablaNom, $datos, &$sentencia){

      $sentencia = "insert into ".$tablaNom." (".implode(",",array_keys($datos)).") values "." (:".implode(", :",$datos).")";


    }
    $insercion="insert into tabla (campos) values (valores)";
    insertV2($tabla,$coche,$insercion);
    var_dump($insercion);
      ?>




    
</body>
</html>