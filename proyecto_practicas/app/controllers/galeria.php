<?php
//requires

////Cargar paquetes instalados con composer
require 'vendor/autoload.php';
//utils
require_once  "utils/utils.php";
require_once "utils/File.php";
//Database
require_once "database/Connection.php";
require_once "database/QueryBuilder.php";
require_once "database/IEntity.php";
//Core
require_once "core/App.php";
//Core
require_once "exceptions/FileException.php";
require_once "exceptions/QueryException.php";
require_once "exceptions/AppException.php";
//Entity
require_once "entity/ImagenGaleria.php";
require_once "entity/Categoria.php";
//Repository
require_once "repository/ImagenGaleriaRepository.php";
require_once "repository/CategoriaRepository.php";


$mensaje = "Correcto";
$errores = array();

//Crear un archivo de log (app.log)
$logHandler = new Monolog\Handler\StreamHandler('logs/app.log');

// Inicializa el control de logs
$logger = new Monolog\Logger('mi-aplicacion');

//Definir el modo de manejar el log (con el archivo definido antes)
$logger->pushHandler($logHandler);

try {
    
    $connection = App::getConnection();

    $imagenGaleriaRepository = new ImagenGaleriaRepository(); 

    $categoriaRepository = new CategoriaRepository(); 


    if ($_SERVER["REQUEST_METHOD"]==="POST") {

        $descripcion = trim(htmlspecialchars($_POST["descripcion"]));

        $categoria = trim(htmlspecialchars($_POST["categoria"]));
        
        $tiposAceptados = ["image/jpeg", "image/png", "image/gif"];

        $imagen = new File("imagen", $tiposAceptados);
        
        $imagen->saveUploadFile(ImagenGaleria::RUTA_IMAGENES_GALLERY);
        
        $imagen->copyFile(ImagenGaleria::RUTA_IMAGENES_GALLERY, ImagenGaleria::RUTA_IMAGENES_PORTFOLIO);

        $imagenGaleria = new ImagenGaleria($imagen->getFileName(), $descripcion, $categoria);

        $imagenGaleriaRepository->save($imagenGaleria);

        $mensaje = "Datos enviados";

        //Grabar entras de log

        $logger->debug('Entrada');
        
    }
     $arrayImagenes = $imagenGaleriaRepository->findAll(); 

     $categorias = $categoriaRepository->findAll();


} catch (FileException $fileException) {
    $errores [] = $fileException->getMessage();
} catch (QueryException $queryException) {
    $errores [] = $queryException->getMessage();
} catch (AppException $AppException) {
    $errores [] = $AppException->getMessage();
}
    

require __DIR__ . "/../views/galeria.view.php";
