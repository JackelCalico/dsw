<?php
include __DIR__ . "/partials/inicio-doc.part.php";
include __DIR__ . "/partials/nav.part.php"; 
?>
<!-- Principal Content Start -->
<div id="galeria">
    <div class="container">
        <div class="col-xs-12 col-sm-8 col-sm-push-2">
            <h1>GALERÍA</h1>
            <hr>
            <?php if ($_SERVER['REQUEST_METHOD'] === 'POST') : ?>
            <div class="alert alert-<?= empty($errores) ? 'info' : 'danger'; 
            //Se crea un div en el cual se muestran los errores hayados
            
            ?> alert-dismissible" role="alert">
                <button type="button" class="clase" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>

                <?php 
                
                //Comprueba que no existan errores
                if(empty($errores)) : ?>
                <p><?= $mensaje ?></p>
                <?php 
                //Si existen los mete en un array y los imprime con un foreach
                
                else : ?>
                <ul>
                    <?php foreach($errores as $error) : ?>
                    <li><?= $error ?></li>
                    <?php endforeach; ?>
                </ul>
                <?php endif; ?>
            </div>
            <?php endif; ?>

            <form class="form-horizontal" method="POST"
                enctype="multipart/form-data">
                <div class="form-group">
                    <div class="col-xs-12">
                        <label class="label-control">Imagen</label>
                        <input class="form-control-file" name="imagen" type="file">
                    </div>
                </div>
                <label class="label-control">Categoría</label>
                   <select class="form-control" name="categoria">
                     <?php foreach ($categorias as $categoria) : ?>
                     <option value="<?= $categoria->getId(); ?>"><?= $categoria->getNombre(); ?></option>
                     <?php endforeach; ?>
                   </select>
                <div class="form-group">
                    <div class="col-xs-12">
                        <label class="label-control">Descripción</label>
                        <textarea class="form-control" name="descripcion" value="<?= $descripcion ?>"></textarea>
                        <button class="pull-right btn btn-lg sr-button">ENVIAR</button>
                    </div>
                </div>
            </form>
        </div>
        <table class="table">
          <thead>
            <tr>
              <th scope="col-2">Imagen</th>
              <th scope="col">Nombre</th>
              <th scope="col">Descripción</th>
              <th scope="col">Categoría</th>
              <th scope="col">Visualizaciones</th>
              <th scope="col">Likes</th>
              <th scope="col">Downloads</th>
            </tr>
          </thead>
          <tbody>
            <?php
            foreach ($arrayImagenes as $key) {?>
            <tr>
              
              <td>
              <img src="<?= ImagenGaleria::RUTA_IMAGENES_GALLERY.$key->getNombre(); ?>" alt="<?= $key->getDescripcion(); ?>" class="foto200px"style="max-width:30%">
              </td>
              <td><?= $key->getNombre(); ?></td>
              <td><?= $key->getDescripcion(); ?></td>
              <td><?= $imagenGaleriaRepository->getCategoria($key)->getNombre() ?></td>
              <td><?= $key->getNumVisualizaciones(); ?></td>
              <td><?= $key->getNumLikes(); ?></td>
              <td><?= $key->getNumDownloads(); ?></td>
            </tr>
            <?php } ?>
          </tbody>
      </table>
    </div>
</div>
<!-- Principal Content End -->
<?php include __DIR__ . "/partials/fin-doc.part.php"; ?>