<?php 

return [

 "dsw/proyecto_practicas" => "app/controllers/index.php",

 "dsw/proyecto_practicas/about" => "app/controllers/about.php",

 "dsw/proyecto_practicas/asociados" => "app/controllers/asociados.php",

 "dsw/proyecto_practicas/blog" => "app/controllers/blog.php",

 "dsw/proyecto_practicas/contact" => "app/controllers/contact.php",

 "dsw/proyecto_practicas/galeria" => "app/controllers/galeria.php",

 "dsw/proyecto_practicas/post" => "app/controllers/single_post.php"

]

?>