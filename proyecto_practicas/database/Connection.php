<?php 

class Connection{

  public static function make() {

        
        //print_r ($config);
        try {
            $config = App::get("config")["database"];
            $connection = new PDO(
            $config["connection"].";dbname=".$config["name"],
            $config["username"],
            $config["password"],
            $config["options"]
          );
          //print_r ($connection);
        } catch (PDOException $PDOException) {

          throw new PDOException("No se ha podido conectar con la BBDD");
         
          }
        return $connection;
      }
    }

      

?>