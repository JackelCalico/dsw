<?php   

require_once "database/IEntity.php";

class Publicacion implements IEntity{

    private $id;
    private $imagen;
    private $categoria;
    private $titulo;
    private $entrada;
    private $contenido;
    private $autor;
    private $fecha;
    

    function __construct($imagen = "", $categoria = "",$titulo = "",$entrada = "", $contenido = "",$autor = "",$fecha = ""){

        $this->id = 0;
        $this->imagen = $imagen;
        $this->categoria = $categoria;
        $this->titulo = $titulo;
        $this->entrada = $entrada;
        $this->contenido = $contenido;
        $this->autor = $autor;
        $this->fecha = null;
        

    }

    

    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of imagen
     */ 
    public function getImagen()
    {
        return $this->imagen;
    }

    /**
     * Set the value of imagen
     *
     * @return  self
     */ 
    public function setImagen($imagen)
    {
        $this->imagen = $imagen;

        return $this;
    }

    /**
     * Get the value of categoria
     */ 
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * Set the value of categoria
     *
     * @return  self
     */ 
    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;

        return $this;
    }

    /**
     * Get the value of titulo
     */ 
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set the value of titulo
     *
     * @return  self
     */ 
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get the value of contenido
     */ 
    public function getContenido()
    {
        return $this->contenido;
    }

    /**
     * Set the value of contenido
     *
     * @return  self
     */ 
    public function setContenido($contenido)
    {
        $this->contenido = $contenido;

        return $this;
    }

    /**
     * Get the value of fecha
     */ 
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set the value of fecha
     *
     * @return  self
     */ 
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get the value of autor
     */ 
    public function getAutor()
    {
        return $this->autor;
    }

    /**
     * Set the value of autor
     *
     * @return  self
     */ 
    public function setAutor($autor)
    {
        $this->autor = $autor;

        return $this;
    }

    public function toArray(): array
    {
      return [
        "id"=>$this->getId(),
       "imagen"=>$this->getImagen(),
       "categoria"=>$this->getCategoria(),
       "titulo"=>$this->getTitulo(),
       "entrada"=>$this->getEntrada(),
       "contenido"=>$this->getContenido(),
       "autor"=>$this->getAutor()

      ];
    }


    /**
     * Get the value of entrada
     */ 
    public function getEntrada()
    {
        return $this->entrada;
    }

    /**
     * Set the value of entrada
     *
     * @return  self
     */ 
    public function setEntrada($entrada)
    {
        $this->entrada = $entrada;

        return $this;
    }
}
?>