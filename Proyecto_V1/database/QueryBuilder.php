<?php

abstract class QueryBuilder
{
    private $connection;

    private $table;

    private $classEntity;

    public function __construct(string $table, string $classEntity)
    {
        $this->connection = App::getConnection();

        $this->table = $table;

        $this ->classEntity = $classEntity;
    }

    //FindAll Intenta ejecutar una consulta que muestre todo lo que contiene una tabla en forma de array

    public function findAll()
    {
        $sql = "select * from $this->table";
        $pdoStatement = $this->executeQuery($sql);
        return $pdoStatement;
    }

    //Realiza una consulta sobre un valor en especifico, en este caso al campo id

    public function FindOne($valor){

        $sql = "select * from $this->table where id = $valor";
        $pdoStatement = $this->executeQuery($sql);
        return $pdoStatement;
    }

    //Ejecuta una consulta al campo titulo 

    public function findBuscador($valor){
        $sql = "select * from $this->table where titulo = '$valor'";
        $pdoStatement = $this->executeQuery($sql);
        return $pdoStatement;

    }

    //Consulta al campo Categoria

    public function findCategoria($valor){
        $sql = "select * from $this->table where categoria = '$valor'";
        $pdoStatement = $this->executeQuery($sql);
        return $pdoStatement;

    }

    

//Inserta los datos recogidos
    public function save(IEntity $entity): void
    {
        try {
            $parameters = $entity->toArray();//Convierte la clase a un array

            $sql = sprintf(
     "insert into %s (%s) values (%s)",// Devuelve un string formateado

            $this->table,
        implode(", ", array_keys($parameters)),
            ":". implode(", :", array_keys($parameters))
 );

            $statement = $this->connection->prepare($sql);

            var_dump ($statement);

            $statement->execute($parameters);
        } catch (PDOException $exception) {
            throw new QueryException("Error al insertar en la BBDD.");
        }
    }

    public function executeQuery(string $sql): array
    {
        $pdoStatement = $this->connection->prepare($sql);

        if ($pdoStatement->execute()===false) {
            throw new QueryException("No se ha podido ejecutar la consulta");
        }

        return $pdoStatement->fetchAll(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, $this->classEntity);
    }


}
