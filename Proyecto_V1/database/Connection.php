<?php 

class Connection{

public static function make() {

        $config = App::get("config")["database"];
        try {
          $pdo = new PDO(
            $config['connection'].';dbname='.$config['name'],
            $config['username'],
            $config['password'],
            $config['options'],
            //$pdo::ATTR_ERRMODE=>PDO::ERRMODE_EXCEPTION
          );
          
        } catch (PDOException $PDOException) {

          throw new PDOException("No se ha podido conectar con la BBDD");
         
          }
        return $pdo;
      }
    }

      

?>