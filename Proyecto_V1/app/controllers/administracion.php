<?php 

//Database
require_once "database/IEntity.php";
require_once "database/Connection.php";
require_once "database/QueryBuilder.php";

//entity
require_once "entity/Publicacion.php";
require_once "entity/Categoria.php";

//App
require_once "core/App.php";

//Exceptions

require_once "exceptions/QueryException.php";
require_once "exceptions/AppException.php";
require_once "exceptions/FileException.php";

//Utils
require_once "utils/utils.php";
require_once "utils/File.php";

//Repositorios
require_once "repository/PublicacionRepository.php";

require_once "repository/CategoriaRepository.php";

//Monolog
require 'vendor/autoload.php';

$archivoDestino = "dsw/Proyecto_V1/administracion";

$errores = Array();

//Crear un archivo de log (app.log)
$logHandler = new Monolog\Handler\StreamHandler('logs/app.log');

// Inicializa el control de logs
$logger = new Monolog\Logger('mi-aplicacion');

//Definir el modo de manejar el log (con el archivo definido antes)
$logger->pushHandler($logHandler);


//Conexion a la base de datos
try {
    
    $connection = App::getConnection();

    $categoriaRepository = new CategoriaRepository();

    $publi = new PublicacionRepository();

    $categoriasArray = $categoriaRepository->findAll();

    if ($_SERVER["REQUEST_METHOD"]==="POST") {
        $categoria = trim(htmlspecialchars($_POST["categoria"]));
        $contenido = trim(htmlspecialchars($_POST["contenido"]));
        $tiposAceptados = ["image/jpeg", "image/png", "image/gif"];
        $imagen = new File("imagen", $tiposAceptados);
        $autor = trim(htmlspecialchars($_POST["autor"]));
        $titulo = trim(htmlspecialchars($_POST["titulo"]));
        $entrada = trim(htmlspecialchars($_POST["entrada"]));
        $mensaje = "Datos enviados";

        echo $autor;
        

        $imagen->saveUploadFile("images/publicaciones/");

        $nombre = "images/publicaciones/".$imagen->getFileName();


        $publicacion = new Publicacion($nombre,$categoria,$titulo,$entrada,$contenido,$autor);

        print_r ($publicacion);

        $publi->save($publicacion);

        //Grabar entras de log

        $logger->debug('Entrada');
    }   



} catch (QueryException $queryException) {
    $errores [] = $queryException->getMessage();
}

catch (AppException $appException) {

    $errores [] = $appException->getMessage();

}





//Vistas
require_once "app/views/administracion-view.php";

?>