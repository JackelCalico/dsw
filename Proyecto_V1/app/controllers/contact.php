<?php

//Utils
require_once "utils/utils.php";


$nombre = htmlentities($_POST['nombre'] ?? "nombreComodin");
$apellidos = htmlentities($_POST['apellidos'] ?? "apellidosComodin");
$email = htmlentities($_POST['email'] ?? "emailComodin@comodin.com");
$asunto = htmlentities($_POST['asunto'] ?? "asuntoComodin");
$mensaje = htmlentities($_POST['mensaje'] ?? "mensajeComodin");
$error = "";




$errores = array();

if ($nombre !== null) {
    if (trim($nombre) == '') {
        $error = "campo vacio en Nombre";
        array_push($errores, $error);
    }

    if(is_numeric($nombre) == true){
        $error = "campo numerico en Nombre no valido";
        array_push($errores, $error);

    }
}


if ($email !== null) {
    if (trim($email) == '') {
        $error = "campo vacio en email";
        array_push($errores, $error);
    }

    else if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $error = "Correo no valido";
            array_push($errores, $error);
        }
    }



if ($asunto !== null) {
    if (trim($asunto) == '') {
        $error = "campo vacio en asunto";
        array_push($errores, $error);
    }
}


if ($mensaje !== null) {
    if (trim($asunto) == '') {
        $error = "campo vacio en mensaje";
        array_push($errores, $error);
    }
}





//Vista
require_once "app/views/contact-view.php";
