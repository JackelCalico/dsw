<?php

//Database
require_once "database/IEntity.php";
require_once "database/Connection.php";
require_once "database/QueryBuilder.php";

//entity
require_once "entity/Publicacion.php";
require_once "entity/Categoria.php";


//Utils
require_once "utils/utils.php";

//App
require_once "core/App.php";


//Exceptions

require_once "exceptions/QueryException.php";
require_once "exceptions/AppException.php";


//Repositorios
require_once "repository/PublicacionRepository.php";
require_once "repository/CategoriaRepository.php";

//Conexion a la base de datos
try {

    //Se pide la id para saber que letra a sido la escogida

    $id = $_GET['id'];
    
    
    $connection = App::getConnection();

    $publicacionRepository = new PublicacionRepository();

    //consulta a una publicacion en especifico

    $letraArray = $publicacionRepository->findOne($id);
    
    


} catch (QueryException $QueryException) {
    $errores [] = $queryException->getMessage();
}

catch (AppException $appException) {
    $errores [] = $appException->getMessage();
}

catch (PDOException $PDOException) {
    $errores [] = $appException->getMessage();
}




//Vista
require_once "app/views/letraPublicacion-view.php";
