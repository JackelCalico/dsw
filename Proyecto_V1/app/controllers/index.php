<?php

//Database
require_once "database/IEntity.php";
require_once "database/Connection.php";
require_once "database/QueryBuilder.php";

//entity
require_once "entity/Publicacion.php";
require_once "entity/Categoria.php";


//Utils
require_once "utils/utils.php";

//App
require_once "core/App.php";


//Exceptions

require_once "exceptions/QueryException.php";
require_once "exceptions/AppException.php";


//Repositorios
require_once "repository/PublicacionRepository.php";
require_once "repository/CategoriaRepository.php";

//Conexion a la base de datos
try {

    
    $connection = App::getConnection();

    $publicacionRepository = new PublicacionRepository();

    //Consulta a la base de datos de las publicaciones

    $publicacionesArray = $publicacionRepository->findAll();

    $categoriaRepository = new CategoriaRepository();

    //Consulta a la base de datos de las categorias

    $categoriaArray = $categoriaRepository->findAll();

    
    
//Excepciones

} catch (QueryException $QueryException) {
    $errores [] = $queryException->getMessage();
}

catch (AppException $appException) {
    $errores [] = $appException->getMessage();
}

catch (PDOException $PDOException) {
    $errores [] = $PDOException->getMessage();
}




//Vista
require_once "app/views/index-view.php";
