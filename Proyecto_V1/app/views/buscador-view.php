<?php

require_once "partials/nav-partial.php";
?>

<!-- Page Content -->
<div class="container">

<div class="row">

  <!-- Blog Entries Column -->
  <div class="col-md-8">

    <h1 class="my-4">News
      <small>Turkish translations</small>
    </h1>

    <?php require_once "partials/entrada-partial.php"; ?>

    

    <!-- Pagination -->
    <ul class="pagination justify-content-center mb-4">
      <li class="page-item">
        <a class="page-link" href="#">&larr; Older</a>
      </li>
      <li class="page-item disabled">
        <a class="page-link" href="#">Newer &rarr;</a>
      </li>
    </ul>

  </div>

<?php
require_once "partials/aside-partial.php";
require_once "partials/footer-partial.php";
?>