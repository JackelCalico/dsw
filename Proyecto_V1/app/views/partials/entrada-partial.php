
<?php
foreach ($publicacionesArray ?? [] as $publicacion) {

  //var_dump($publicacionesArray);
    ?>
<!-- Blog Post -->
<div class="card mb-4">
        <?php
        if($publicacion->getImagen() != null){
        ?> 
       <img class="card-img-top" src="<?= $publicacion->getImagen()?>" alt="Card image cap">
       <?php
        }
        ?>
      <div class="card-body">
        <h2 class="card-title"><?= $publicacion->getTitulo()?></h2>
        <p class="card-text"><?= $publicacion->getEntrada()?></p>
        <a href="letra?id=<?= $publicacion->getId()?>" class="btn btn-primary">Read More &rarr;</a>
      </div>
      <div class="card-footer text-muted">
      <?= $publicacion->getFecha()?> by
        <a href=""><?= $publicacion->getAutor()?></a>
      </div>
    </div>

    <?php
  }
    ?>
