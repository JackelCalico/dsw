<!-- Sidebar Widgets Column -->
<div class="col-md-4">

<!-- Search Widget -->

<form action="buscador" method="post">
<div class="card my-4">
  <h5 class="card-header">Buscador</h5>
  <div class="card-body">
    <div class="input-group">
      <input type="text" class="form-control" placeholder="Search for..." name="busqueda">
      <span class="input-group-btn">
        <button class="btn btn-secondary">Go!</button>
      </span>
    </div>
  </div>
</div>
</form>

<!-- Categories Widget -->
<div class="card my-4">
  <h5 class="card-header">Categorias</h5>
  <div class="card-body">
    <div class="row">
      <div class="col-lg-6">
        <ul class="list-unstyled mb-0">
          <?php foreach($categoriaArray as $cat){?>
          <li>
            <a href="busquedaCategorias?valor=<?= $cat->getId()?>" class="col-6"><?=$cat->getNombre()?></a>
          </li>
          <?php }?>
        </ul>
          </div>
    </div>
  </div>
</div>

</div>

</div>
<!-- /.row -->

</div>
<!-- /.container -->