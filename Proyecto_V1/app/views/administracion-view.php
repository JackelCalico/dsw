<?php
require_once "partials/nav-partial.php";
?>
<div id="administracion">
  <div class="container">
    <div class="col-xs-12 col-sm-8 col-sm-push-2">
      <br>
      <h1>Administración<h1>
    </div>
  
    <form class="form-horizontal" action="administracion" method="POST"
                enctype="multipart/form-data">
                <div class="form-group">
                    <div class="col-xs-12">
                        <label class="label-control">Imagen</label>
                        <input class="form-control-file" name="imagen" type="file">
                    </div>
                </div>
                   <label class="label-control">Categoría</label>
                   <select class="form-control" name="categoria">
                     <?php foreach ($categoriasArray as $categoria) :?>
                     <option value="<?= $categoria->getId(); ?>"><?= $categoria->getNombre(); ?></option>
                     <?php endforeach; ?>
                   </select>
                   <div class="col-xs-12">
                        <label class="label-control">Autor</label>
                        <input type="text" class="form-control" name="autor"></input>
                    </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <label class="label-control">Titulo</label>
                        <input type="text" class="form-control" name="titulo"></textarea>
                    </div>
                    <div class="form-group">
                    <div class="col-xs-12">
                        <label class="label-control">Entrada</label>
                        <input type="text" class="form-control" name="entrada"></textarea>
                    </div>
                    <div class="form-group">
                    <div class="form-group">
                    <div class="col-xs-12">
                        <label class="label-control">Contenido</label>
                        <textarea class="form-control" name="contenido"></textarea> 
                        <br>
                        </div>
                        <div class="form-group">
                    <button class="btn btn-warning">ENVIAR</button>
                </div>
            </form>
    <div>
      <div>
      </div>
    </div>
  </div>
  <?php

?>